<?php

// $adminPrefix = env("ADMIN_PREFIX", 'admin');

// Route::group(['prefix' => $adminPrefix], function () {

Route::get("/test", function () {
  return "banana";
})->name("test");

Route::get("/banana", '\\Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin\AuthController@banana')->name("banana");

Route::post('login', ['uses' => '\\Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin\AuthController@login', 'as' => 'login']);
Route::get('login/authenticate/{token}', [
  'as' => 'login.authenticate',
  'uses' => '\\Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin\AuthController@authenticateEmail'
]);
// });