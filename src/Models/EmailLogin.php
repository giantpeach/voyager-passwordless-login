<?php

namespace Giantpeach\VoyagerPasswordlessLogin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EmailLogin extends Model
{
    //
    protected $fillable = ['email', 'token'];

    public function user() {
        return $this->hasOne(\App\User::class, 'email', 'email');
    }

    public static function createTokenForEmail($email)
    {
        return self::create([
            'email' => $email,
            'token' => Str::random(255)
        ]);
    }

    public static function validateToken($token) 
    {
        return self::where("token", $token)
            ->where('created_at', '>', Carbon::parse('-15 minutes'))
            ->firstOrFail();
    }
}
