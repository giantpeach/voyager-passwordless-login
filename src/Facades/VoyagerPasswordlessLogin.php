<?php

namespace Giantpeach\VoyagerPasswordlessLogin\Facades;

use Illuminate\Support\Facades\Facade;

class VoyagerPasswordlessLogin extends Facade {
  protected static function getFacadeAccessor() {
    return "voyager-passwordless-login";
  }
}