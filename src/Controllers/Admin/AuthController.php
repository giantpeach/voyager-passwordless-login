<?php

namespace Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin;

use Giantpeach\VoyagerPasswordlessLogin\Controllers\Controller;
use Giantpeach\VoyagerPasswordlessLogin\Models\EmailLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Facades\Voyager;

class AuthController extends Controller
{
    //

    public function login(Request $request)
    {
        $this->validate($request, ['email' => 'required|email|exists:users']);

        $emailLogin = EmailLogin::createTokenForEmail($request->input('email'));

        $url = route('login.authenticate', [
            'token' => $emailLogin->token
        ]);

        Mail::send('voyager-passwordless-login::auth.emails.email-login', ['url' => $url], function ($m) use ($request) {
            $m->from(Voyager::setting('site.email_address', 'noreply@myapp.com'), Voyager::setting('site.title', 'Voyager'));
            $m->to($request->input('email'))->subject(sprintf('%s Login', Voyager::setting('site.title', 'Voyager')));
        });

        return 'Login email sent. Go check your email.';
    }

    public function banana()
    {
        return view("voyager-passwordless-login::banana");
    }

    public function authenticateEmail($token)
    {
        $emailLogin = EmailLogin::validateToken($token);

        Auth::login($emailLogin->user);

        // Probably should remove token after logged in
        EmailLogin::destroy($emailLogin->id);

        if ($emailLogin->user->can("browse_admin")) {
            return redirect("admin");
        } else {
            return redirect("home");
        }
    }
}
