<?php

namespace Giantpeach\VoyagerPasswordlessLogin;

use Illuminate\Support\ServiceProvider;

class VoyagerPasswordlessLoginServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('voyager-passwordless-login', function () {
      return new VoyagerPasswordlessLogin();
    });
  }

  public function boot()
  {
    // $this->app->make('Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin\AuthController');
    $this->loadMigrationsFrom(__DIR__ . "/../database/migrations");
    $this->loadViewsFrom(__DIR__ . "/../resources/views", "voyager-passwordless-login");
    // $this->loadFactoriesFrom(__DIR__ . "/../database/factories");

    $this->publishes([
      __DIR__ . '/../resources/views' => resource_path('views/vendor/voyager-passwordless-login'),
    ]);
  }
}
