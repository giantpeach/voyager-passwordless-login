<?php

namespace Giantpeach\VoyagerPasswordlessLogin;

class VoyagerPasswordlessLogin {
  public function __construct()
  {
    
  }
  
  public function routes() {
    include __DIR__.'/routes.php';
  }
}