<?php

namespace Giantpeach\VoyagerPasswordlessLogin\Tests\Unit;

use Giantpeach\VoyagerPasswordlessLogin\Controllers\Admin\AuthController;
use Giantpeach\VoyagerPasswordlessLogin\Tests\Stubs\User;
use Giantpeach\VoyagerPasswordlessLogin\Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class ExampleTest extends TestCase
{

  public function testBasicTest()
  {

    require __DIR__ . "../../../src/routes.php";

    $response = $this->get("test");
    $response->assertStatus(200);
    // $this->assertTrue(true);
  }

  /**
   * Sends an invalid email and expects a ValidationException in response
   */
  public function testInvalidEmail()
  {
    require __DIR__ . "../../../src/routes.php";

    Mail::fake();

    factory(User::class)->create();

    $request = Request::create("/login", 'POST', [
      'email' => 'sdfsdgsdg'
    ]);

    $controller = new AuthController();

    $this->expectException(ValidationException::class);

    $controller->login($request);
  }

  public function testValidEmail()
  {
    require __DIR__ . "../../../src/routes.php";

    Mail::fake();

    factory(User::class)->create();

    $request = Request::create("/login", 'POST', [
      'email' => 'tom@giantpeach.agency'
    ]);

    $controller = new AuthController();

    $response = $controller->login($request);

    $this->assertStringContainsStringIgnoringCase($response, 'Login email sent. Go check your email.');
  }

  // public function testAuthenticateEmail()
  // {
  //   $controller = new AuthController();

  //   factory(User::class)->create();

  //   $response = $controller->authenticateEmail("sdsdfsdfsd");
  // }

  public function testAnotherTest()
  {
    $this->assertTrue(true);
  }
}
