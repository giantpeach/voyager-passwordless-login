<?php

namespace Giantpeach\VoyagerPasswordlessLogin\Tests;

use Giantpeach\VoyagerPasswordlessLogin\VoyagerPasswordlessLoginServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestCase extends \Orchestra\Testbench\TestCase
{
  use RefreshDatabase;

  public function setUp(): void
  {
    parent::setUp();
    $this->withFactories(__DIR__ . '/factories');
    $this->loadLaravelMigrations();
    $this->loadMigrationsFrom(__DIR__ . "/../src/database/migrations");
    $this->artisan('migrate')->run();

  }

  protected function getPackageProviders($app)
  {
    return [
      VoyagerPasswordlessLoginServiceProvider::class,
    ];
  }

  protected function getPackageAliases($app)
  {
    return [
      'VoyagerPasswordlessLogin' => 'Giantpeach\VoyagerPasswordlessLogin\Facades\VoyagerPasswordlessLogin'
    ];
  }

  protected function getEnvironmentSetUp($app)
  {
  }
}
