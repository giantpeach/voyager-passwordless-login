<?php

use Giantpeach\VoyagerPasswordlessLogin\Tests\Stubs\User;
use Illuminate\Support\Str;

$factory->define(User::class, function (Faker\Generator $faker) {
  static $password;

  return [
    'name' => $faker->name,
    // 'username' => $faker->unique()->userName,
    'email' => "tom@giantpeach.agency",
    'password' => $password ?: $password = bcrypt('secret'),
    'email_verified_at' => now(),
    'remember_token' => Str::random(10),
  ];
});